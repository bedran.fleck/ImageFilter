/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;

/**
 *
 * @author andre
 */
public class ArquivoPGMDeEscrita extends DadosPGM{
    private File file;

    public ArquivoPGMDeEscrita() {
        this.file = null;
    }

    public ArquivoPGMDeEscrita(File file) {
        this.file = file;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
    
    public int escreverArquivo(String path) throws IOException{
        
        
        if (this.file == null) {
            this.file = new File(path);
            if (this.file == null) {
                return -1;
            }
        }
        
        FileWriter fs = new FileWriter(this.file);
        
        try (BufferedWriter writer = new BufferedWriter(fs)) {
            byte[] pixels = this.getPixels();
            int largura = this.getLargura();
            int altura = this.getAltura();
            int maximo = this.getMaximo();
            String P5 = this.getP5();
            String comentario = this.getComentario();
            
            System.out.println(largura);
            System.out.println(altura);
            System.out.println(maximo);
            System.out.println(P5);
            
            writer.write(P5);
            writer.newLine();
            writer.write(String.valueOf(largura));
            writer.write(" ");
            writer.write(String.valueOf(altura));
            writer.newLine();
            writer.write(String.valueOf(maximo));
            writer.newLine();
            String pixelsToWrite = new String(pixels);
            System.out.println(pixels);
            
            writer.close();
            
            Path p = this.file.toPath();
            
            
            Files.write(p, pixels, StandardOpenOption.APPEND);
            
        }
        
        return 0;
    }
//    CONTINUAR DAQUI
}
