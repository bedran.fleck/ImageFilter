/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author andre
 */
public class ArquivoPGMDeLeitura extends DadosPGM{

    private File file;
    
    public ArquivoPGMDeLeitura() {
        this.setFile(null);
    }

    public ArquivoPGMDeLeitura(File file) {
        this.file = file;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
    
    public int lerArquivo() throws FileNotFoundException, IOException{
        int altura, largura, maximo;
        String P5, comentario;
        
        InputStream f = new FileInputStream(this.file);
        BufferedReader d = new BufferedReader(new InputStreamReader(f));
            P5 = d.readLine();    // first line contains P2 or P5
            String line = d.readLine();     // second line contains height and width
            while (line.startsWith("#")) {
                comentario = line;
                line = d.readLine();
            }
            Scanner s = new Scanner(line);
            largura = s.nextInt();
            altura = s.nextInt();
            line = d.readLine();// third line contains maxVal
            s = new Scanner(line);
            maximo = s.nextInt();
            byte[] im = new byte[altura*largura];

            int count = 0;
            int b = 0;
            byte x = (byte) 0xFF;
            
            for(int i = 0; i < largura; i++){
		for(int j = 0; j < altura; j++){
                    b = d.read();
                    
                    if(b < 0){
                        break;
                    }
                    if(b == '\n'){
                    }
                    else{
                        
                        im[i*largura+j] = (byte)(b >> 8 & 0xFF);
                    
                    }
		}
            }
            
        
//        Scanner scan = new Scanner(this.file);
//        
//        P5 = scan.nextLine();
//        if (P5 == null) {
//            return -1;
//        }
//        if(scan.findInLine("#") == null){
//            scan.nextLine();
//        } else {
//            comentario = "#"+scan.nextLine();
//        }
//        
//        largura = scan.nextInt();
//        altura = scan.nextInt();
//        maximo = scan.nextInt();
        this.setLargura(largura);
        this.setAltura(altura);
        this.setMaximo(maximo);
        this.setPixels(im);
        this.setP5(P5);
//        
//        scan.nextLine();
//        
//        ArrayList<Character> pixels = new ArrayList<Character>();
//        
//        for(int i = 0; i < largura; i++){
//		for(int j = 0; j < altura; j++){
//                    pixels.set(i*largura+j, scan.next().charAt(0));
//		}
//	}
        return 0;
    }
}
