/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;

/**
 *
 * @author andre
 */
public class DadosPGM {
    private String P5, comentario;
    private int largura, altura, maximo;
    private byte[] pixels;

    public DadosPGM() {
    }

    public DadosPGM(String P5, String comentario, int largura, int altura, int maximo, byte[] pixels) {
        this.P5 = P5;
        this.comentario = comentario;
        this.largura = largura;
        this.altura = altura;
        this.maximo = maximo;
        this.pixels = pixels;
    }

    public String getP5() {
        return P5;
    }

    public String getComentario() {
        return comentario;
    }

    public int getLargura() {
        return largura;
    }

    public int getAltura() {
        return altura;
    }

    public int getMaximo() {
        return maximo;
    }

    public byte[] getPixels() {
        return pixels;
    }

    public void setP5(String P5) {
        this.P5 = P5;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public void setLargura(int largura) {
        this.largura = largura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public void setMaximo(int maximo) {
        this.maximo = maximo;
    }

    public void setPixels(byte[] pixels) {
        this.pixels = pixels;
    }
    
    
    
}
