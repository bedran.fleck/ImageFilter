/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;

/**
 *
 * @author andre
 */
public class Filter {
    private int div, size;
    private ArrayList<Integer> matriz;

    public Filter() {
        this.div = 1;
        this.size = 3;
    }

    public Filter(int div, int size) {
        this.div = div;
        this.size = size;
    }

    public ArrayList<Integer> getMatriz() {
        return matriz;
    }

    public void setMatriz(ArrayList<Integer> matriz) {
        this.matriz = matriz;
    }

    public int getDiv() {
        return div;
    }

    public void setDiv(int div) {
        this.div = div;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public byte[] aplicaFiltro(byte[] pixels, int largura, int altura, int maximo){
        
        int value, i, j;
	byte[] novos_pixels = new byte[largura*altura];
	if(novos_pixels == null){
		return null;
	}
        
        for(i=0; i<size/2; i++){
		for(j=0; j<largura; j++){
                    
                    novos_pixels[i+j*largura] = pixels[i+j*largura];
                    novos_pixels[(i+j*largura)+largura-1] = pixels[(i+j*largura)+largura-1];
		
		}
	}

	for(i=0; i<size/2; i++){
		for(j=0; j<altura; j++){
                    
                    novos_pixels[i*altura+j] = pixels[i*altura+j];
                    novos_pixels[(j-altura*i)+(altura-1)*largura] = pixels[(j-altura*i)+(altura-1)*largura];

                        
		}
	}

	for(i=size/2; i<largura-size/2; i++){
		for(j=size/2; j<altura-size/2; j++){
			value = 0;
			for(int x=-(size/2); x<=size/2; x++){
				for(int y=-(size/2); y<=size/2;y++){
					value += matriz.get((x+1)+size*(y+1)) * pixels[(i+x)+(y+j)*largura];
				}
			}
				value /= div;

				value = value < 0 ? 0 : value;
				value = value > 255 ? 255 : value;

				if(value > maximo)
					maximo = value;

				novos_pixels[i+j*largura] = (byte)value;
		}
	}

	return novos_pixels;
        
    }
    
    
}
