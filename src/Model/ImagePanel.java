/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 *
 * @author andre
 */
public class ImagePanel extends JPanel{
    
    private BufferedImage image;
    
    
    public ImagePanel(File file) {
       try {                
          image = ImageIO.read(file);
       } catch (IOException ex) {
           System.out.println("ERROR 404");
       }
    }
    
    
    
    public void paintComponent(Graphics g, int x, int y) {
        super.paintComponent(g);
        g.drawImage(image, x, y, null); // see javadoc for more info on the parameters            
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }
    
}
