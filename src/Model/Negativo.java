/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;

/**
 *
 * @author andre
 */
public class Negativo extends Filter{

    public Negativo() {
        super.setDiv(1);
        super.setSize(3);
    }
    
    public byte[] aplicaFiltro(byte[] pixels, int largura, int altura, int maximo){
        int i, j;
	byte[] novos_pixels = new byte[largura*altura];
	if(novos_pixels == null){
		return null;
	}
        
        for(i=0; i<largura; i++){
		for(j=0; j<altura; j++){
                    novos_pixels[i+j*largura] = (byte)(maximo - pixels[i+j*largura]);
		}
	}

	return novos_pixels;
    }
    
}
