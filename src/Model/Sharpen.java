/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;

/**
 *
 * @author andre
 */
public class Sharpen extends Filter{

    public Sharpen() {
        super.setDiv(1);
        super.setSize(3);
        
        ArrayList<Integer> matriz = new ArrayList<Integer>();
        
        for(int i=0; i<9; i++){
		matriz.add((-i)%2);
		if(i == 4)
			matriz.add(5);
	}
        
        super.setMatriz(matriz);
    }
    
}
